module Main where

main :: IO ()
main = getAllReserved 7 1

getAllReserved boardLength currPos = let result = foldl (\a b -> whileToNothing boardLength currPos a b) [] [reserveR, reserveL, reserveU, reserveD, reserveUL, reserveUR, reserveDL, reserveDR] 
                                     in print result

-- reserveR :: RowLength -> CurrentPosition -> Place
reserveL :: Int -> Int -> Maybe Int
reserveL lenBoard queen = let posInRow = queen `mod` lenBoard
                              posNextStep = (queen - 1) `mod` lenBoard
                          in if posNextStep >= posInRow then Nothing else Just $ queen -1

reserveR :: Int -> Int -> Maybe Int
reserveR lenBoard queen = let posInRow = queen `mod` lenBoard
                              posNextStep = (queen + 1) `mod` lenBoard
                          in if posNextStep <= posInRow then Nothing else Just $ queen + 1

reserveU :: Int -> Int -> Maybe Int
reserveU lenBoard queen = let posInRow = queen `mod` lenBoard
                              posNextStep = queen + lenBoard
                          in if posNextStep < 0 || posNextStep > (lenBoard * lenBoard) then Nothing else Just $ queen + lenBoard

reserveD :: Int -> Int -> Maybe Int
reserveD lenBoard queen = let posInRow = queen `mod` lenBoard
                              posNextStep = queen - lenBoard
                          in if posNextStep < 0 || posNextStep > (lenBoard * lenBoard) then Nothing else Just $ queen - lenBoard

reserveUL lenBoard queen = case reserveL lenBoard queen of
                            Nothing -> Nothing
                            Just queen' -> reserveU lenBoard queen'

reserveDL lenBoard queen = case reserveL lenBoard queen of
                            Nothing -> Nothing
                            Just queen' -> reserveD lenBoard queen'

reserveUR lenBoard queen = case reserveR lenBoard queen of
                            Nothing -> Nothing
                            Just queen' -> reserveU lenBoard queen'

reserveDR lenBoard queen = case reserveR lenBoard queen of
                            Nothing -> Nothing
                            Just queen' -> reserveD lenBoard queen'

whileToNothing :: Int -> Int -> [Int] -> (Int -> Int -> Maybe Int) -> [Int]
whileToNothing lenBoard queen result fv = let res = fv lenBoard queen
                                    in case res of
                                        Nothing -> result
                                        Just r -> let result' = result ++ [r]
                                                    in whileToNothing lenBoard r result' fv
