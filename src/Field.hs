module Field where

newtype Row = Row Int deriving (Eq, Show)
newtype Column = Column Int deriving (Eq, Show)

data Field = Field Row Column deriving (Eq, Show)
