module Queen where

import Field
type Position = Maybe Int
type Reversed = [Field]
type QueenId = Int
type IsOk = Bool

data Queen = Queen QueenId Position Reversed deriving (Show)

mkEmptyQueen id = Queen id Nothing [] 