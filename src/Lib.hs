module Lib
    ( someFunc
    ) where

import Board
import Queen

someFunc :: IO ()
someFunc = print $ mkBoard $ [mkEmptyQueen i|i <- [1..7]]