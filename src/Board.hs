module Board where

import Data.Map
import Field
import Queen

type FieldCount = Int

type Fields = Map FieldCount Field
type Queens = [Queen]

data Board = Board Fields Reversed Queens deriving (Show)

mkBoard :: Queens -> Board
mkBoard queens = Board mkFields [] queens
  where
    mkFields = let lineLength = length queens
                   countOfBoard = lineLength * lineLength
                   in fromList $ [(i, Field (Row ((div (i-1) lineLength) + 1)) (Column ((mod (i-1) lineLength) + 1))) | 
                                              i <- [1..countOfBoard]]
